﻿using UnityEngine;
using System.Collections;

public class OrcController : MonoBehaviour 
{
	public EnemyController enemyController;
	//Animator anim;

	public GameObject projectile;

	// Use this for initialization
	void Start () 
	{
		//anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	public void Attack()
	{
		if (enemyController.currentTarget == null)
		{
			return;
		}

		enemyController.attackTimer = enemyController.attackCooldown;

		Vector2 direction = (Vector2)enemyController.currentTarget.transform.position - (Vector2)transform.position;
		float angle = Mathf.Atan2 (direction.y, direction.x) * Mathf.Rad2Deg;
		Quaternion rotation = Quaternion.AngleAxis (angle, Vector3.forward);
		
		ProjectileController p = ((GameObject)Instantiate(projectile, transform.position, rotation)).GetComponent<ProjectileController>();
		p.SetDirection(direction);
	}

	public void FinishAttack()
	{
		enemyController.FinishAttacking();
	}

}
