﻿using UnityEngine;
using System.Collections;

public class MapController : MonoBehaviour 
{
	Zones currentZone;

	public GameObject[] maps;
	public static GameObject screenSprite;

	GameObject baseTower;

	bool faded = false;

	// Use this for initialization
	void Start() 
	{
		screenSprite = GameObject.Find ("ScreenFade");
		baseTower = GameObject.Find ("Base");
	}
	
	// Update is called once per frame
	void Update() 
	{
		if (baseTower == null &&
		    !faded)
		{
			faded = true;
			FadeToBlack();
		}
	}

	public void CompleteZone()
	{
		if ((int)currentZone > maps.Length - 1)
		{
			return;
		}

		Animator anim = maps[(int)currentZone].GetComponent<Animator>();

		anim.SetTrigger("Finish");

		currentZone += 1;
	}

	static void FadeToBlack()
	{
		Animator anim = screenSprite.GetComponent<Animator>();
		ScreenFaderController.Lose ();
		anim.SetTrigger("Lose");
	}

	public enum Zones
	{
		One = 0,
		Two = 1,
		Three = 2,
		Four = 3
	}
}
