﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour 
{
	public float speed = 5f;

	//Transform myTransform;

	float attackTimer = 0;
	public float attackSpeed = 1f;

	bool attacking = false;

	Animator anim;

	public AxeController axe;

	// Use this for initialization
	void Start() 
	{
		//myTransform = gameObject.GetComponent<Transform>();
		anim = gameObject.GetComponentInChildren<Animator>();

		attackTimer = attackSpeed;

		Coins.Reset();
	}
	
	// Update is called once per frame
	void Update()
	{
		FaceMouse();
		CheckForAttack();
	}

	void FixedUpdate()
	{
		if (attackTimer > 0)
		{
			attackTimer -= Time.fixedDeltaTime;
		}
		Move();
	}

	void Move()
	{
		float stepSpeed = speed * Time.fixedDeltaTime;
		
		Vector2 direction = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));

		direction.Normalize();

		Vector2 movement = (Vector2)transform.position + direction * stepSpeed;
		anim.SetFloat("speed", direction.magnitude);
		rigidbody2D.MovePosition(movement);
	}

	void FaceMouse()
	{
		Vector2 mousePosition =  Camera.main.ScreenToWorldPoint(new Vector2(Input.mousePosition.x, Input.mousePosition.y));
		Vector2 direction = mousePosition - (Vector2)transform.position;
		float angle = Mathf.Atan2 (direction.y, direction.x) * Mathf.Rad2Deg;
		angle += 270;
		transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
	}

	public void FinishAttacking()
	{
		attacking = false;
		attackTimer = attackSpeed;
		axe.EndSwing();
	}

	void CheckForAttack()
	{
		if (attacking ||
		    attackTimer > 0)
		{
			return;
		}

		if (Input.GetMouseButton(0) == true)
		{
			axe.StartSwing();
			attacking = true;
			anim.SetTrigger("attack");
		}
	}
}