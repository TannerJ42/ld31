﻿using UnityEngine;
using System.Collections;

public class NodeController : MonoBehaviour
{
	GameObject towerMenu;

	public GameObject buildingAnimPrefab;

	public GameObject towerMenuLocation;

	public GameObject mgPrefab;
	public GameObject cPrefab;
	public GameObject tPrefab;

	bool Occupied = false;
	bool TowerBuilt = false;

	GameObject tower = null;

	// Use this for initialization
	void Start() 
	{
		towerMenu = GameObject.Find("Build Icons");
	}

	void Update()
	{
		if (Occupied &&
		    !TowerBuilt)
		{
			if (Input.GetKeyDown (KeyCode.Alpha1))
			{
				if (Coins.Amount >= Coins.MGCost)
				{
					Build(mgPrefab);
					Coins.Amount -= Coins.MGCost;
				}
			}
			else if (Input.GetKeyDown (KeyCode.Alpha2))
			{
				if (Coins.Amount >= Coins.CCost)
				{
					Build(cPrefab);
					Coins.Amount -= Coins.CCost;
				}
			}
//			else if (Input.GetKeyDown (KeyCode.Alpha3))
//			{
//				if (Coins.Amount >= Coins.TCost)
//				{
//					Build(tPrefab);
//					Coins.Amount -= Coins.TCost;
//				}
//			}
		}

		if (TowerBuilt &&
		    tower == null)
		{
			TowerBuilt = false;
			this.collider2D.enabled = true;

		}
	}

	void OnTriggerEnter2D(Collider2D c)
	{
		if (c.tag != "Player" ||
		    c.isTrigger)
		{
			Physics2D.IgnoreCollision(c.collider2D, transform.collider2D);
			return;
		}

		Occupied = true;

		if (towerMenu != null)
		{
			towerMenu.SetActive(true);
			towerMenu.transform.position = towerMenuLocation.transform.position;
		}
	}

	void OnTriggerExit2D(Collider2D c)
	{
		if (c.tag != "Player" ||
		    c.isTrigger)
		{
			Physics2D.IgnoreCollision(c.collider2D, transform.collider2D);
			return;
		}

		Occupied = false;

		if (towerMenu != null)
		{
			towerMenu.SetActive(false);
		}
	}

	void Build(GameObject prefab)
	{
		GameObject buildingAnimation = (GameObject)Instantiate(buildingAnimPrefab, this.transform.position, this.transform.rotation);
		tower = (GameObject)Instantiate(prefab, this.transform.position, this.transform.rotation);
		this.collider2D.enabled = false;
		Occupied = false;
		towerMenu.SetActive(false);
		TowerBuilt = true;

		BuildingAnimationManager manager = buildingAnimation.GetComponentInChildren<BuildingAnimationManager>();
		manager.building = tower;
		tower.SetActive(false);
	}
}
