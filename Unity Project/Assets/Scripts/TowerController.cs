﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class TowerController : MonoBehaviour 
{
	List<EnemyController> enemies;

	public float attackTimer = 0;
	public float attackInterval = 1f;

	//public GameObject projectile;

	public int maxHP = 100;
	public int currentHP;

	public GameObject currentTarget;

	Animator anim;

	public HealthBarController healthBar;



	public bool isAlive
	{
		get
		{
			return currentHP > 0;
		}
	}

	// Use this for initialization
	void Start()
	{
		enemies = new List<EnemyController>();
		currentHP = maxHP;
		anim = GetComponentInChildren<Animator>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		FindTarget();

		if (currentTarget != null)
		{
			FaceTarget((Vector2)currentTarget.transform.position);
		}
	}

	void FixedUpdate()
	{
		if (!isAlive)
		{
			return;
		}

		if (attackTimer > 0)
		{
			attackTimer -= Time.fixedDeltaTime;
		}

//		if (attackTimer <= 0)
//		{
//			TryToAttack();
//		}
	}

	void OnTriggerEnter2D(Collider2D c)
	{
		if (c.gameObject == null)
			return;

		if (c.tag != "Enemy" ||
		    c.isTrigger ||
		    enemies == null)
		{
			return;
		}

		EnemyController controller = c.GetComponent<EnemyController>();

		if (!enemies.Contains(controller))
		{
			enemies.Add(controller);
		}
	}

	void OnTriggerExit2D(Collider2D c)
	{
		if (c.gameObject == null)
			return;

		if (c.tag != "Enemy" ||
		    enemies == null)
		{
			return;
		}

		EnemyController controller = c.GetComponent<EnemyController>();

		if (enemies.Contains(controller))
		{
			enemies.Remove(controller);
		}
	}

	void FindTarget()
	{
		EnemyController target = null;
		float closest = float.MaxValue;

		for(int i = enemies.Count - 1; i >= 0; i--)
		{
			if (enemies[i] == null ||
			    !enemies[i].isAlive)
			{
				enemies.RemoveAt(i);
			}
			else
			{
				float distance = Vector2.Distance(transform.position, enemies[i].transform.position);

				if (distance < closest &&
				    enemies[i].isAlive)
				{
					target = enemies[i];
					closest = distance;
				}
			}
		}

		if (target != null)
			currentTarget = target.gameObject;
	}

	void FaceTarget(Vector2 faceTarget)
	{
		if (faceTarget == Vector2.zero)
			return;

		Vector2 direction = faceTarget - (Vector2)transform.position;
		float angle = Mathf.Atan2 (direction.y, direction.x) * Mathf.Rad2Deg;
		angle += 270;
		transform.rotation = Quaternion.AngleAxis (angle, Vector3.forward);
	}

	public void takeDamage(int damage, Vector2 location)
	{
		if (this.name == "Base")
		{
			CameraShake.Shake();
		}

		if (!isAlive)
		{
			return;
		}

		currentHP -= damage;

		if (healthBar != null)
		{
			healthBar.SetDamage(currentHP/(float)maxHP);
		}
		
		if (currentHP <= 0)
		{
			Destroy(this.gameObject);
		}
	}
}
