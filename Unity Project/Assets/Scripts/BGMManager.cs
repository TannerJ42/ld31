﻿using UnityEngine;
using System.Collections;

public class BGMManager : MonoBehaviour 
{
	public AudioClip startingBGM;
	public AudioClip nearDeathBGM;
	public AudioSource source;

	TowerController baseTower;

	bool danger = false;
	public float changePercent = 0.2f;
	float bar;

	// Use this for initialization
	void Start () 
	{
		baseTower = GameObject.Find ("Base").GetComponent<TowerController>();
		source.clip = startingBGM;

		source.Play ();

		bar = changePercent * baseTower.maxHP;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (!danger &&
		    baseTower.currentHP < bar)
		{
			danger = true;
			source.Stop();
			source.clip = nearDeathBGM;
			source.Play();
		}
	}
}
