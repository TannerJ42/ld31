﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AxeController : MonoBehaviour 
{
	public int damage = 50;

	bool swinging = false;

	List<GameObject> enemiesHit;

	// Use this for initialization
	void Start()
	{
		
	}
	
	// Update is called once per frame
	void Update()
	{
		
	}

	public void StartSwing()
	{
		swinging = true;
		enemiesHit = new List<GameObject>();
	}

	public void EndSwing()
	{
		swinging = false;
		enemiesHit = null;
	}

	void OnTriggerEnter2D(Collider2D c)
	{
		if (c.gameObject == null)
			return;

		if (swinging &&
			c != null &&
		    c.tag == "Enemy" &&
		    !c.isTrigger)
		{
			if (enemiesHit.Contains(c.gameObject))
			{
				return;
			}

			enemiesHit.Add (c.gameObject);
			EnemyController e = c.gameObject.GetComponent<EnemyController>();
			e.takeDamage(damage, this.transform.position);
		}
	}
}
