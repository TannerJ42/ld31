﻿using UnityEngine;
using System.Collections;

public class CannonController : MonoBehaviour 
{
	public GameObject projectile;
	TowerController towerController;
	Animator anim;

	// Use this for initialization
	void Start() 
	{
		anim = GetComponentInChildren<Animator>();
		towerController = transform.parent.parent.GetComponent<TowerController>();
	}
	
	// Update is called once per frame
	void Update() 
	{
		if (towerController.attackTimer <= 0 &&
		    towerController.currentTarget != null)
		{
			anim.SetTrigger("Attack");
				Attack(towerController.currentTarget.transform);
		}
	}

	public void FinishAttacking()
	{

	}

	void Attack(Transform targetTransform)
	{
		towerController.attackTimer = towerController.attackInterval;
		
		Vector2 direction = (Vector2)targetTransform.position - (Vector2)transform.position;
		float angle = Mathf.Atan2 (direction.y, direction.x) * Mathf.Rad2Deg;
		Quaternion rotation = Quaternion.AngleAxis (angle, Vector3.forward);
		
		ProjectileController p = ((GameObject)Instantiate(projectile, transform.position, rotation)).GetComponent<ProjectileController>();
		p.SetDirection(direction);
	}
}
