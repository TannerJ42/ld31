﻿using UnityEngine;
using System.Collections;

public class HUDController : MonoBehaviour 
{
	int currentCoins;
	public TextMesh coinAmount;

	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (currentCoins != Coins.Amount)
		{
			currentCoins = Coins.Amount;
			coinAmount.text = currentCoins.ToString ();
		}


	}
}
