﻿using UnityEngine;
using System.Collections;

public class WaveManager : MonoBehaviour 
{
	public GameObject[] wave1spawners;
	public GameObject[] wave2spawners;
	public GameObject[] wave3spawners;
	public GameObject[] wave4spawners;
	
	GameObject[] currentSpawners;

	int currentWave = 0;

	float timer;

	public float initialDelay = 10f;
	public float timeBetweenWaves = 15f;

	public GameObject screenSprite;

	MapController mapManager;

	State state = State.Waiting;

	bool finished = false;

	// Use this for initialization
	void Start () 
	{
		timer = initialDelay;

		mapManager = GameObject.Find("MapManager").GetComponent<MapController>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		switch (state)
			{
		case State.Waiting:
			timer -= Time.deltaTime;
		
			if (timer <= 0)
			{
				NextWave();
			}
			break;
		case State.Active:
			if (isWaveFinished())
			{
				state = State.Waiting;
				timer = timeBetweenWaves;
			}
			break;
		}

		if (currentWave == 4 &&
		    !finished)
		{
			CheckForWin();
		}
	}

	void NextWave()
	{
		switch (currentWave)
		{
		case 0:
			Debug.Log("Starting wave 1.");
			currentWave += 1;
			StartWave(ref wave1spawners);
			break;
		case 1:
			Debug.Log("Starting wave 2.");
			currentWave += 1;
			StartWave(ref wave2spawners);
			mapManager.CompleteZone();
			break;
		case 2:
			Debug.Log("Starting wave 3.");
			currentWave += 1;
			StartWave(ref wave3spawners);
			mapManager.CompleteZone();
			break;
		case 3:
			Debug.Log("Starting wave 4.");
			currentWave += 1;
			StartWave(ref wave4spawners);
			mapManager.CompleteZone();
			break;
		default:
			break;
		}
	}

	void StartWave(ref GameObject[] spawners)
	{
		state = State.Active;

		if (spawners == null)
		{
			return;
		}

		currentSpawners = spawners;

		for (int i = 0; i <= spawners.Length - 1; i++)
		{
			spawners[i].SetActive(true);
		}
	}

	bool isWaveFinished()
	{
		if (currentSpawners == null)
		{
			return true;
		}

		foreach (GameObject g in currentSpawners)
		{
			if (g != null)
			{
				return false;
			}
		}
		return true;
	}

	void CheckForWin()
	{
		int length = GameObject.FindGameObjectsWithTag("Enemy").Length;

		if (length == 0 &&
		    isWaveFinished())
		{
			finished = true;
			//ScreenFaderController.Win();		
			Animator anim = screenSprite.GetComponent<Animator>();
			
			anim.SetTrigger("Win");
		}
	}

	enum State
	{
		Waiting,
		Active,
	}
}
