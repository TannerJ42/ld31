﻿using UnityEngine;
using System.Collections;

public class MachineGunController : MonoBehaviour 
{
	TowerController towerController;
	Animator anim;

	public int damage;
	
	// Use this for initialization
	void Start () 
	{
		anim = GetComponentInChildren<Animator>();
		towerController = GetComponentInChildren<TowerController>();
	}
	
	// Update is called once per frame
	void Update()
	{
		if (towerController.attackTimer <= 0
		    && towerController.currentTarget != null)
		{
			MachineGunDamage();
		}

		anim.SetBool("Attacking", towerController.currentTarget != null);
	}

	void MachineGunDamage()
	{
		if (towerController.currentTarget == null)
		{
			return;
		}

		towerController.attackTimer = towerController.attackInterval;

		EnemyController ec = towerController.currentTarget.GetComponent<EnemyController>();

		if (ec == null)
		{
			return;
		}

		ec.takeDamage(damage, ec.gameObject.transform.position - transform.position);
	}
}
