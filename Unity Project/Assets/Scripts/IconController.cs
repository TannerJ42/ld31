﻿using UnityEngine;
using System.Collections;

public class IconController : MonoBehaviour 
{
	public BuildMenu buildMenu;
	public string towerType;

	void Start ()
	{
		if (buildMenu != null &&
		    towerType == "MG")
		{
			buildMenu.HoverOver (towerType);
		}
	}

	// Update is called once per frame
	void Update () 
	{

	}

	void OnMouseDown()
	{
		buildMenu.TryToBuy(towerType);
	}

	void OnMouseEnter()
	{
		buildMenu.HoverOver(towerType);
	}
}
