﻿using UnityEngine;
using System.Collections;

public class ProjectileController : MonoBehaviour 
{
	public float speed = 5f;
	public int damage = 10;
	public string targetTag = "Enemy";

	Vector2 direction = Vector2.up;

	// Use this for initialization
	void Start () 
	{
		Destroy(this.gameObject, 5f);
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}


	void FixedUpdate()
	{
		Vector2 movement = (Vector2)transform.position + direction * speed * Time.fixedDeltaTime;

		transform.position = new Vector3(movement.x, movement.y, transform.position.z);
	}

	public void SetDirection(Vector2 newDirection)
	{
		direction.Normalize();

		direction = newDirection;
	}

	void OnTriggerEnter2D(Collider2D c)
	{
		if (c == null ||
		    c.tag != targetTag ||
		    c.isTrigger)
		{
			return;
		}

		if (c.tag == "Enemy")
		{
			EnemyController e = c.GetComponent<EnemyController>();
			e.takeDamage(damage, (Vector2)c.transform.position);
		}

		if (c.tag == "Tower")
		{
			TowerController t = c.GetComponent<TowerController>();
			t.takeDamage(damage, (Vector2)c.transform.position);
		}

		Destroy(this.gameObject);
	}
}
