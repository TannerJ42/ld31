﻿using UnityEngine;
using System.Collections;

public class Coins : MonoBehaviour 
{
	public static int Amount = 60;
	public static int MGCost = 15;
	public static int CCost = 20;
	public static int TCost = 30;

	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{

	}

	public static void Reset()
	{
		Amount = 60;
	}

	public static void AddCoins(int amount)
	{
		Amount += amount;
	}

	public static void Create()
	{

	}
}
