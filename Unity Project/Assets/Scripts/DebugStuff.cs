﻿using UnityEngine;
using System.Collections;

public class DebugStuff : MonoBehaviour 
{
	MapController map;

	// Use this for initialization
	void Start () 
	{
		map = GameObject.Find ("MapManager").GetComponent<MapController>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (Input.GetKeyDown (KeyCode.Keypad1))
		{
			Debug.Log ("Debug!");
			map.CompleteZone();
		}
	}
}
